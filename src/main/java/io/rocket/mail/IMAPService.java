package io.rocket.mail;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

public class IMAPService {

	
	public Store getStore() throws MessagingException {
		Properties prop = new Properties();
		prop.setProperty("mail.smtp.socketFactory.port", "465");
		prop.setProperty("mail.smtp.host", "smtp.gmail.com");
		prop.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		prop.setProperty("mail.smtp.auth", "true");
		prop.setProperty("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(prop, null);
		Store store = session.getStore("imaps");
		store.connect("smtp.gmail.com", "joey.osseman@gmail.com","87041670");
		return store;
	}
}
