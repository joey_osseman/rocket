package io.rocket.mail.email;

public class Email {
	private String subject;
	private String summary;
	private boolean unread;
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	public boolean isUnread() {
		return unread;
	}
	
	public void setUnread(boolean unread) {
		this.unread = unread;
	}
}
