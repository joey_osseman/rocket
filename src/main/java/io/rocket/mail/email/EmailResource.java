package io.rocket.mail.email;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/email")
public class EmailResource {
	
	@Inject
	private EmailService emailService;
	
    @GET
    @Path("/")
    @Produces({ "application/json" })
    public List<Email> single() {
    	return emailService.getEmails();
    }
}
