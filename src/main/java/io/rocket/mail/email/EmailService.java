package io.rocket.mail.email;

import io.rocket.mail.IMAPService;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.inject.Inject;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Flags.Flag;

public class EmailService {

	@Inject IMAPService imap;
	
	public List<Email> getEmails() {
		try {
			Folder inbox = imap.getStore().getFolder("inbox");
			inbox.open(Folder.READ_ONLY);
			Message[] messages = inbox.getMessages(1,100);
			List<Email> emails = new ArrayList<Email>(messages.length);
			for(Message m : messages) {
				Email email = new Email();
				email.setSubject(m.getSubject());
				email.setUnread(!m.isSet(Flag.SEEN));
				email.setSummary(m.getDescription());
				emails.add(email);
			}
			
			return emails;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ArrayList<Email>();
	}
}
