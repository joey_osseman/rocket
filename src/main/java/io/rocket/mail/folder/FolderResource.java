package io.rocket.mail.folder;

import io.rocket.mail.email.Email;
import io.rocket.mail.email.EmailService;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/email/folder")
public class FolderResource {
	
	@Inject
	private FolderService folderService;
	
    @GET
    @Path("/")
    @Produces({ "application/json" })
    public List<MailDirectory> single() {
    	return folderService.getFolders();
    }
}