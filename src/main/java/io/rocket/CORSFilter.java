package io.rocket;


import javax.ws.rs.container.*;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;

@Provider
public class CORSFilter implements ContainerResponseFilter, ContainerRequestFilter {


    @Override
    public void filter( ContainerRequestContext requestCtx ) throws IOException {
        requestCtx.getHeaders().add( "Access-Control-Allow-Origin", "*" );
        ///requestCtx.
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        responseContext.getHeaders().add( "Access-Control-Allow-Origin", "*" );

        if(requestContext.getHeaders().get("Access-Control-Request-Headers") != null) {
            String o = "";
            for (String ob : requestContext.getHeaders().get("Access-Control-Request-Headers")) {
                if (o.equals("")) {
                    o = ob;
                } else {
                    o += ", " + ob;
                }
            }
            responseContext.getHeaders().add("Access-Control-Allow-Headers", o);
        }
    }
}
