package io.rocket.authentication;

public class Session {
	private int id;
	private User user;
	
	public Session(User user) {
		this.setUser(user);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
