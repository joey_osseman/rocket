package io.rocket.authentication;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.rocket.authentication.Session;
import io.rocket.authentication.User;


@Path("/login")
public class LoginResource {
	
    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Session single(Credentials credentials) {
    	if(credentials != null 
    			&& credentials.getUsername() != null && credentials.getPassword() != null 
    			&& credentials.getUsername().equals("joey") && credentials.getPassword().equals("joey")) {		
	    	Session s = new Session(new User());
	    	s.setId(2);
	    	s.getUser().setId(21);
	    	s.getUser().setUsername("Joey");
	    	return s;
    	}
    	
    	return null;
    }
}