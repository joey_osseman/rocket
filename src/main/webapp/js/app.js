window.require.define({"addons/menu": function(exports, require, module) {
angular.module("menu", function(){}).directive('menuItem', function($location) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs, controller) {
      element.attr('href', attrs.menuItem);
      var path = attrs.menuItem.substring(1);
      scope.location = $location;
      scope.$watch('location.path()', function(newPath) {
        if (path === newPath) {
          element.addClass("active");
        } else {
          element.removeClass("active");
        }
      });
    }
  }
}).service("menuState", function() {
  var name = "menuState"
  return {
    get : function() {
      return (localStorage.getItem(name) == "true");
    },
    set : function(value) {
      localStorage.setItem(name, value);
    }
  };
});
}});


window.require.define({"modules/agenda/controllers/AgendaIndexController": function(exports, require, module) {
angular.module('nl.osseman.agenda').controller('AgendaIndexController', ['$scope', function($scope) {
  var date = new Date();
var d = date.getDate();
var m = date.getMonth();
var y = date.getFullYear();

$scope.changeTo = 'Hungarian';
/* event source that pulls from google.com */
$scope.eventSource = {
        url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
        className: 'gcal-event',           // an option!
        currentTimezone: 'America/Chicago' // an option!
};
/* event source that contains custom events on the scope */
$scope.events = [
  {title: 'All Day Event',start: new Date(y, m, 1)},
  {title: 'Long Event',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
  {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
  {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
  {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
  {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
];
/* event source that calls a function on every view switch */
$scope.eventsF = function (start, end, timezone, callback) {
  var s = new Date(start).getTime() / 1000;
  var e = new Date(end).getTime() / 1000;
  var m = new Date(start).getMonth();
  var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
  callback(events);
};

$scope.calEventsExt = {
   color: '#f00',
   textColor: 'yellow',
   events: [
      {type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
      {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
      {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
    ]
};
/* alert on eventClick */
$scope.alertOnEventClick = function( event, allDay, jsEvent, view ){
    $scope.alertMessage = (event.title + ' was clicked ');
};
/* alert on Drop */
 $scope.alertOnDrop = function( event, revertFunc, jsEvent, ui, view){
   $scope.alertMessage = ('Event Droped on ' + event.start.format());
};
/* alert on Resize */
$scope.alertOnResize = function( event, jsEvent, ui, view){
   $scope.alertMessage = ('Event end date was moved to ' + event.end.format());
};
/* add and removes an event source of choice */
$scope.addRemoveEventSource = function(sources,source) {
  var canAdd = 0;
  angular.forEach(sources,function(value, key){
    if(sources[key] === source){
      sources.splice(key,1);
      canAdd = 1;
    }
  });
  if(canAdd === 0){
    sources.push(source);
  }
};
/* add custom event*/
$scope.addEvent = function() {
  $scope.events.push({
    title: 'Open Sesame',
    start: new Date(y, m, 28),
    end: new Date(y, m, 29),
    className: ['openSesame']
  });
};
/* remove event */
$scope.remove = function(index) {
  $scope.events.splice(index,1);
};
/* Change View */
$scope.changeView = function(view,calendar) {
  calendar.fullCalendar('changeView',view);
};
/* Change View */
$scope.renderCalender = function(calendar) {
  if(calendar){
    calendar.fullCalendar('render');
  }
};
/* config object */
$scope.uiConfig = {
  calendar:{
    height: 450,
    editable: true,
    header:{
      left: 'title',
      center: '',
      right: 'today prev,next'
    },
    eventClick: $scope.alertOnEventClick,
    eventDrop: $scope.alertOnDrop,
    eventResize: $scope.alertOnResize
  }
};

$scope.changeLang = function() {
  if($scope.changeTo === 'Hungarian'){
    $scope.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
    $scope.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
    $scope.changeTo= 'English';
  } else {
    $scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    $scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    $scope.changeTo = 'Hungarian';
  }
};
/* event sources array*/
$scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
$scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];


}]);
}});


window.require.define({"modules/agenda/module": function(exports, require, module) {
angular.module('nl.osseman.agenda', ['ui.router','ui.calendar'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('root.agenda', {
        url: "/agenda",
        controller: "AgendaIndexController",
        templateUrl: "agenda/views/index.html",
        data : {
          authorizedRoles: []
        }
      });
  });
}});


window.require.define({"modules/authentication/controllers/LoginController": function(exports, require, module) {
angular.module('nl.osseman.authentication').controller('LoginController', function ($scope, $rootScope, AUTH_EVENTS, AuthService) {
  $scope.credentials = {
    username: '',
    password: ''
  };
  $scope.login = function (credentials) {
    AuthService.login(credentials).then(function (user) {
      console.log("test2");
      $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
      $scope.setCurrentUser(user);
    }, function () {
      swipe();
      $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
    });
  };
}).run(function ($rootScope, AUTH_EVENTS, AuthService) {
  $rootScope.$on('$stateChangeStart', function (event, next) {
    var authorizedRoles = false;
    try {
      authorizedRoles = next.data.authorizedRoles;
    } catch(e){}
    if (authorizedRoles && !AuthService.isAuthorized(authorizedRoles)) {
      event.preventDefault();
      if (AuthService.isAuthenticated()) {

        // user is not allowed
        $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
      } else {
        console.log("test");
        // user is not logged in
        $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
      }
    }
})}).config(function ($httpProvider) {
  $httpProvider.interceptors.push([
    '$injector',
    function ($injector) {
      return $injector.get('AuthInterceptor');
    }
  ]);
}).factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
  return {
    responseError: function (response) {
      $rootScope.$broadcast({
        401: AUTH_EVENTS.notAuthenticated,
        403: AUTH_EVENTS.notAuthorized,
        419: AUTH_EVENTS.sessionTimeout,
        440: AUTH_EVENTS.sessionTimeout
      }[response.status], response);
      return $q.reject(response);
    }
  };
}).directive('formAutofillFix', function ($timeout) {
  return function (scope, element, attrs) {
    element.prop('method', 'post');
    if (attrs.ngSubmit) {
      $timeout(function () {
        element
          .unbind('submit')
          .bind('submit', function (event) {
            event.preventDefault();
            element
              .find('input, textarea, select')
              .trigger('input')
              .trigger('change')
              .trigger('keydown');
            scope.$apply(attrs.ngSubmit);
          });
      });
    }
  };
});
}});


window.require.define({"modules/authentication/logic/AUTH_EVENTS": function(exports, require, module) {
angular.module('nl.osseman.authentication').constant('AUTH_EVENTS', {
  loginSuccess: 'auth-login-success',
  loginFailed: 'auth-login-failed',
  logoutSuccess: 'auth-logout-success',
  sessionTimeout: 'auth-session-timeout',
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
}).constant('USER_ROLES', {
  all: '*',
  admin: 'admin',
  editor: 'editor',
  guest: 'guest'
});
}});


window.require.define({"modules/authentication/logic/AuthService": function(exports, require, module) {
angular.module('nl.osseman.authentication').factory('AuthService', function ($http, Session, baseurl) {
  var authService = {};

  authService.login = function (credentials) {
    return $http
      .post(baseurl + '/rest/login/', credentials)
      .then(function (res) {
        Session.create(res.data.id, res.data.user.id,
                       res.data.user.role);
        return res.data.user;
      });
  };

  authService.isAuthenticated = function () {
    return !!Session.userId;
  };

  authService.isAuthorized = function (authorizedRoles) {
    if (!angular.isArray(authorizedRoles)) {
      authorizedRoles = [authorizedRoles];
    }
    return (authService.isAuthenticated() && (
      authorizedRoles.indexOf(Session.userRole) !== -1 || authorizedRoles.length == 0));
  };

  return authService;
})
}});


window.require.define({"modules/authentication/logic/Session": function(exports, require, module) {
angular.module('nl.osseman.authentication').service('Session', function () {
  this.create = function (sessionId, userId, userRole) {
    this.id = sessionId;
    this.userId = userId;
    this.userRole = userRole;
  };
  this.destroy = function () {
    this.id = null;
    this.userId = null;
    this.userRole = null;
  };
  return this;
});
}});


window.require.define({"modules/authentication/module": function(exports, require, module) {
angular.module('nl.osseman.authentication', ['ui.router'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
        url: "/login",
        controller: "LoginController",
        templateUrl: "authentication/views/login.html"
      });


  });
}});


window.require.define({"modules/contacts/controllers/DashboardController": function(exports, require, module) {
angular.module('nl.osseman.contacts').controller('ContactIndexController', ['$scope', function($scope) {
  $scope.contacts = [];
}]);
}});


window.require.define({"modules/contacts/module": function(exports, require, module) {
angular.module('nl.osseman.contacts', ['ui.router'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('root.dashboard', {
        url: "/",
        controller: "DashboardController",
        templateUrl: "dashboard/views/dashboard.html",
        data : {
          authorizedRoles: []
        }
      });
  });
}});


window.require.define({"modules/dashboard/controllers/DashboardController": function(exports, require, module) {
angular.module('nl.osseman.dashboard').controller('DashboardController', ['$scope', function($scope) {

  $scope.emails = [{title : "test"}];
  $scope.appointments = [{title : "test"}];
}]);
}});


window.require.define({"modules/dashboard/module": function(exports, require, module) {
angular.module('nl.osseman.dashboard', ['ui.router', 'nl.osseman.authentication'], function(){}).config(
  function($stateProvider, $urlRouterProvider, USER_ROLES) {
    $stateProvider
      .state('root.dashboard', {
        url: "/",
        controller: "DashboardController",
        templateUrl: "dashboard/views/dashboard.html",
        data: {
          authorizedRoles: []
        }
      });
  });
}});


window.require.define({"modules/email/controllers/EmailDetailsController": function(exports, require, module) {
angular.module('nl.osseman.email').controller('EmailDetailsController', ['$scope', function($scope) {
}]);
}});


window.require.define({"modules/email/controllers/EmailIndexController": function(exports, require, module) {
angular.module('nl.osseman.email').controller('EmailIndexController', ['$scope', '$resource', 'baseurl', function($scope, $resource, baseurl) {
  var Email = $resource(baseurl + '/rest/email/:emailId', {emailId:'@id'});
Email.query(function(data) {
    $scope.emails = data;
  });

  /*  $scope.emails = [{
      _id: "randomid",
      subject: "I'm unread",
      summary: "I am an unread e-mail. You should read me...",
      tags: [],
      unread: true,
      dateReceived: new Date(),
      attachments: [
        {filename : "blabla.xlsx"}
      ]
    }, {
        _id: "randomid",
        subject: "Please approve invoice",
        summary: "Hello, I urge you approve my invoice immediately.",
        tags: [],
        unread: false,
        dateReceived: new Date()
      }, {
        _id: "randomid",
        subject: "Acceptance Test",
        summary: "Hello, some attachments I am sending you... Try downloading them..!",
        tags: [],
        unread: false,
        dateReceived: new Date(),
        attachments: [
          {filename : "blabla.xlsx"}
        ]
      }
    ];*/

    $scope.selectAllChange = function() {
        for(var i = 0; i < $scope.emails.length; i++) {
          $scope.emails[i].selected = $scope.selectAll;
        }
    };

    $scope.selectedChange = function(item){
      if(!item.selected && $scope.selectAll) {
        $scope.selectAll = false;
      } else if(item.selected && !$scope.selectAll) {
        var allSelected = true;
        for(var i = 0; i < $scope.emails.length; i++) {
          if(!$scope.emails[i].selected) {
            allSelected = false;
          }
        }

        $scope.selectAll = allSelected;
      }
    };

    $scope.downloadAttachment = function(attachment) {
      alert("'Downloading' "+ attachment.filename + "\r\n\r\n\r\n....Not really.");
    }
}]);
}});


window.require.define({"modules/email/module": function(exports, require, module) {
angular.module('nl.osseman.email', ['ui.router'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('root.email', {
        url: "/email",
        controller: "EmailIndexController",
        templateUrl: "email/views/index.html",
        data : {
          authorizedRoles: []
        }
      });
  });
}});


window.require.define({"modules/errorpages/controllers/ErrorController": function(exports, require, module) {
angular.module('nl.osseman.errorpages').controller('ErrorController', ['$scope', function($scope) {
}]);
}});


window.require.define({"modules/errorpages/module": function(exports, require, module) {
angular.module('nl.osseman.errorpages', ['ui.router'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('root.error', {
        url: "/error/404",
        controller: "ErrorController",
        templateUrl: "errorpages/views/404.html",
        data : {
          authorizedRoles: []
        }
      });
  });
}});


window.require.define({"modules/todo/controllers/EmailIndexController": function(exports, require, module) {
angular.module('nl.osseman.todo').controller('TodoIndexController', ['$scope', '$resource', function($scope, $resource) {
  $scope.todo = [ {
    id : 1,
    title : "Fix my pc",
    done : false
  }, {
    id : 2,
    title : "Make a todo list",
    done : false
  }];
}]);
}});


window.require.define({"modules/todo/module": function(exports, require, module) {
angular.module('nl.osseman.todo', ['ui.router'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('root.todo', {
        url: "/todo",
        controller: "TodoIndexController",
        templateUrl: "todo/views/index.html",
        data : {
          authorizedRoles: []
        }
      });
  });
}});


String.prototype.endsWith = function(suffix) {
	return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
(function() {
	var requireList = window.require.list();
	var conditionalRequire = function(func) {
		var newRequireList = [];
		for(var i = 0; i < requireList.length; i++) {
			if(func(requireList[i]) === true) {
				require(requireList[i]);
			} else {
				newRequireList.push(requireList[i]);
			}
		}

		requireList = newRequireList;
	}

	conditionalRequire(function(s){
		return s.endsWith('module');
	});

	conditionalRequire(function(s){
		return true;
	});
  angular.module('nl.osseman', ['menu', 'nl.osseman.email', 'nl.osseman.todo', 'nl.osseman.authentication', 'nl.osseman.errorpages', 'nl.osseman.agenda', 'nl.osseman.dashboard', 'ngResource', 'ui.bootstrap', 'ui.router']).config(
			function($stateProvider, $urlRouterProvider) {
  				$urlRouterProvider.otherwise("/error/404");
					$stateProvider
						.state('root', {
							abstract: true,
							url: "",
							templateUrl: "master.html"
						});
			}).controller('CoreController', function($scope, menuState, USER_ROLES, AuthService, AUTH_EVENTS, $state) {
				$scope.activeMenuItem = "";
				$scope.isActiveMenuItem = function(item) {
					return $scope.activeMenuItem == item;
				};
				$scope.menuState = menuState.get();
				$scope.toggleMenuState = function() {
					menuState.set(!$scope.menuState);
					$scope.menuState = !$scope.menuState;
				}
				$scope.currentUser = null;
				$scope.userRoles = USER_ROLES;
				$scope.isAuthorized = AuthService.isAuthorized;
				$scope.setCurrentUser = function (user) {
					$scope.currentUser = user;
				};

						$scope.$on(AUTH_EVENTS.notAuthenticated, function(){
							console.log(" go");
							$state.go("login")
						});
						$scope.$on(AUTH_EVENTS.loginSuccess, function(){
							console.log("go2");
							$state.go("root.dashboard")
						});
	}).constant('baseurl', "http://rocket.joeyosseman.nl");


/*
	var App = angular.module('osseman.core',
			[ 'nl.osseman.dashboard', 'nl.osseman.agenda', 'nl.osseman.email','osseman.authentication', 'ngRoute', 'ui.calendar' ]).config(
			function($locationProvider, $routeProvider) {
				$locationProvider.html5Mode(false);
				$routeProvider.otherwise({
			        redirectTo: '/login'
			      });
			}).controller('CoreController', [ function() {

	} ]).directive('ngRedirector', function($location, $rootScope) {
		return function(scope, element, attr) {
				element.on("click", function(event) {
					$rootScope.$apply(function() {
						$location.path(attr.ngRedirector);
					});
				});
		};
	}).constant('BASE_URL', 'http://localhost:8080/api/');
*/
})();


$(document).ready(function(){

$(window).resize(function(){
	//$("#main").css("min-height", function(){
		//return 0;//$(window).height() - $("#main").offset().top;
//});
});


});
