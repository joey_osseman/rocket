module.exports = function (grunt) {
  "use strict";

  // load all grunt tasks
  require('load-grunt-tasks')(grunt);
  grunt.loadNpmTasks('grunt-bower');
  grunt.loadNpmTasks('grunt-config');

  // Default task.
  grunt.registerTask('generateappjs', ['commonjs:app', 'string-replace:baseurl', 'concat:appjs'])
  grunt.registerTask('appcss', ['less:app', 'concat:appcss'])
  grunt.registerTask('vendor', ['clean:vendor', 'bower:vendor', 'copy:custom_vendor', 'concat:vendorjs', 'concat:vendorcss', 'copy:vendor_fonts'])
  grunt.registerTask('app', ['clean:app', 'appcss', 'generateappjs', 'copy:appviews'])
  grunt.registerTask('build', ['clean:dest', 'copy:assets', 'vendor', 'app']);
  grunt.registerTask('default', ['build', 'connect', 'watch']);

  var modRewrite = require('connect-modrewrite');
	var mountFolder = function (connect, dir) {
	    return connect.static(require('path').resolve(dir));
	};


  // Project configuration.
  grunt.initConfig({
		bower : {
			vendor : {
				dest : ".tmp/vendorunused/90_others",
        options : {
					packageSpecific : {
						jquery : {
							dest : ".tmp/vendor/10_jquery"
            },
            "jquery-ui" : {
              dest : ".tmp/vendor/15_jqueryui"
            },
						bootstrap : {
							dest : ".tmp/vendor/16_bootstrap",
							files : [ 'fonts/**/*', 'dist/css/bootstrap.css']
						},
            fontawesome : {
              dest : ".tmp/vendor/17_fontawesome",
              files : [ 'fonts/**/*', 'css/font-awesome.css']
            },
            moment : {
              dest : ".tmp/vendor/20_momentjs"
            },
            angular : {
              dest : ".tmp/vendor/30_angular"
            },
            "angular-route" : {
              dest: ".tmp/vendor/35_angular-route"
            },
            "angular-ui-router" : {
              dest: ".tmp/vendor/34_angular_ui_router"
            },
            "angular-resource" : {
              dest: ".tmp/vendor/36_angular-resource"
            },
            fullcalendar : {
              dest : ".tmp/vendor/40_fullcalendar"
            },
            "angular-ui-calendar" : {
              dest :".tmp/vendor/60_angularcalendar"
            }
          }
        }
      }
    },
    clean : {
			vendor : {
				src : [ ".tmp/vendor" ]
			},
      app : {
        src : [".tmp/app"]
      },
      dest : {

        src : ['<%= grunt.config.get("config.current.target") %>/**/*', '!<%= grunt.config.get("config.current.target") %>/WEB-INF/**']
      },
      appviews : {
        src : ['<%= grunt.config.get("config.current.target") %>/*/**.html', '!<%= grunt.config.get("config.current.target") %>/index.html']
      },
      options: {
        force: true
      }
    },
    concat : {
			vendorjs : {
				dest : '<%= grunt.config.get("config.current.target") %>/js/vendor.js',
				src : ['.tmp/vendor/**/*.js']
			},
      vendorcss : {
        dest : '<%= grunt.config.get("config.current.target") %>/css/vendor.css',
        src : ['.tmp/vendor/**/*.css'],
        options: {
          process: function(src, filepath) {
            return '/* Source: ' + filepath + '*/\n' + src;
          }
        }
      },
			appjs : {
          src : [ '.tmp/app/processedjs/**/*.js' ],
					dest: '<%= grunt.config.get("config.current.target") %>/js/app.js'
			},
			appcss : {
				src : [ '.tmp/app/css/app.css', 'app/**/*.css' ],
				dest : '<%= grunt.config.get("config.current.target") %>/css/app.css'
			}
		},
    commonjs : {
			app : {
				cwd : "app/",
				src : [ "**/*.js", "!app.js" ],
				dest : ".tmp/app/processedjs/"
			}
		},
    less : {
      app: {
        files: {
          ".tmp/app/css/app.css": "app/assets/app.less"
        }
      }
    },
    'string-replace': {
      baseurl: {
        files: {
          '.tmp/app/processedjs/zzz.js': 'app/app.js',
        },
        options: {
          replacements: [ {
              pattern: '<#### BASEURL ####>',
              replacement: '<%= grunt.config.get("config.current.baseurl") %>'
            }
          ]
        }
      }
    },
    copy : {
			custom_vendor : {
        expand: true,
				cwd : 'vendor',
				src: ['**/*'],
				dest : '.tmp/vendor/'
			},
			appviews : {
                expand: true,
				cwd : 'app/modules/',
				src: ['**/*.html'],
				dest : '<%= grunt.config.get("config.current.target") %>/'
			},
			assets : {
				files : [ {
					cwd : 'app/assets/',
					expand : true,
					src : [ '*', '!**/*.css' ],
					dest : '<%= grunt.config.get("config.current.target") %>/',
					filter : 'isFile'
				} ]
			},
			vendor_fonts : {
				flatten : true,
				files : [ {
					flatten : true,
					expand: true,
					src : [ '.tmp/vendor/**/*.eot', '.tmp/vendor/**/*.woff', '.tmp/vendor/**/*.ttf', '.tmp/vendor/**/*.svg', '.tmp/vendor/**/*.woff2' ],
					dest : '<%= grunt.config.get("config.current.target") %>/fonts/'
				} ]
			}
    },
		watch : {
			options : {
				livereload : true
			},
			appjs : {
				files : 'app/**/*.js',
				tasks : [ 'clean:app', 'generateappjs' ],
        options : {
          interrupt : true,
        }
			},
			app_views : {
				files : 'app/modules/**/*.html',
				tasks : ['clean:appviews', 'copy:appviews']
			},
			assets : {
				files : ['app/assets/*', '!app/**/*.css'],
				tasks : [ 'copy:assets' ],
				options : {
					interrupt : true,
				}
			},
			appcss : {
				files : ['app/assets/**/*.less', 'app/*/**.css'],
				tasks : ['appcss'],
				options : {
					interrupt : true,
				}
			}
		},
    connect : {
			options: {
                port: 8000,
            },
            server: {
                options: {
                    middleware: function (connect, options, middlewares) {
                        return [

                            modRewrite (['!\\.html|\\.js|\\.svg|\\.css|\\.png|\\.jpg$ /index.html [L]']),
                            mountFolder(connect, grunt.config.get("config.current.target"))
                        ];
                  }
                }
            }
		}
  });

  var env = "development";
  if(grunt.option("env")) {
    env = grunt.option("env");
  }
  grunt.config.set("config.current", grunt.file.readJSON("config/"+ env + "/config.json"));

  console.log(grunt.config.get("config.current"));
};
