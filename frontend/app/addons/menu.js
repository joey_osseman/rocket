angular.module("menu", function(){}).directive('menuItem', function($location) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs, controller) {
      element.attr('href', attrs.menuItem);
      var path = attrs.menuItem.substring(1);
      scope.location = $location;
      scope.$watch('location.path()', function(newPath) {
        if (path === newPath) {
          element.addClass("active");
        } else {
          element.removeClass("active");
        }
      });
    }
  }
}).service("menuState", function() {
  var name = "menuState"
  return {
    get : function() {
      return (localStorage.getItem(name) == "true");
    },
    set : function(value) {
      localStorage.setItem(name, value);
    }
  };
});
