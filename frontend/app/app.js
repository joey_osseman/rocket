String.prototype.endsWith = function(suffix) {
	return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
(function() {
	var requireList = window.require.list();
	var conditionalRequire = function(func) {
		var newRequireList = [];
		for(var i = 0; i < requireList.length; i++) {
			if(func(requireList[i]) === true) {
				require(requireList[i]);
			} else {
				newRequireList.push(requireList[i]);
			}
		}

		requireList = newRequireList;
	}

	conditionalRequire(function(s){
		return s.endsWith('module');
	});

	conditionalRequire(function(s){
		return true;
	});
  angular.module('nl.osseman', ['menu', 'nl.osseman.email', 'nl.osseman.todo', 'nl.osseman.authentication', 'nl.osseman.errorpages', 'nl.osseman.agenda', 'nl.osseman.dashboard', 'ngResource', 'ui.bootstrap', 'ui.router']).config(
			function($stateProvider, $urlRouterProvider) {
  				$urlRouterProvider.otherwise("/error/404");
					$stateProvider
						.state('root', {
							abstract: true,
							url: "",
							templateUrl: "master.html"
						});
			}).controller('CoreController', function($scope, menuState, USER_ROLES, AuthService, AUTH_EVENTS, $state) {
				$scope.activeMenuItem = "";
				$scope.isActiveMenuItem = function(item) {
					return $scope.activeMenuItem == item;
				};
				$scope.menuState = menuState.get();
				$scope.toggleMenuState = function() {
					menuState.set(!$scope.menuState);
					$scope.menuState = !$scope.menuState;
				}
				$scope.currentUser = null;
				$scope.userRoles = USER_ROLES;
				$scope.isAuthorized = AuthService.isAuthorized;
				$scope.setCurrentUser = function (user) {
					$scope.currentUser = user;
				};

						$scope.$on(AUTH_EVENTS.notAuthenticated, function(){
							console.log(" go");
							$state.go("login")
						});
						$scope.$on(AUTH_EVENTS.loginSuccess, function(){
							console.log("go2");
							$state.go("root.dashboard")
						});
	}).constant('baseurl', "<#### BASEURL ####>");


/*
	var App = angular.module('osseman.core',
			[ 'nl.osseman.dashboard', 'nl.osseman.agenda', 'nl.osseman.email','osseman.authentication', 'ngRoute', 'ui.calendar' ]).config(
			function($locationProvider, $routeProvider) {
				$locationProvider.html5Mode(false);
				$routeProvider.otherwise({
			        redirectTo: '/login'
			      });
			}).controller('CoreController', [ function() {

	} ]).directive('ngRedirector', function($location, $rootScope) {
		return function(scope, element, attr) {
				element.on("click", function(event) {
					$rootScope.$apply(function() {
						$location.path(attr.ngRedirector);
					});
				});
		};
	}).constant('BASE_URL', 'http://localhost:8080/api/');
*/
})();


$(document).ready(function(){

$(window).resize(function(){
	//$("#main").css("min-height", function(){
		//return 0;//$(window).height() - $("#main").offset().top;
//});
});


});
