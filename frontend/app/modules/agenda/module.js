angular.module('nl.osseman.agenda', ['ui.router','ui.calendar'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('root.agenda', {
        url: "/agenda",
        controller: "AgendaIndexController",
        templateUrl: "agenda/views/index.html",
        data : {
          authorizedRoles: []
        }
      });
  });
