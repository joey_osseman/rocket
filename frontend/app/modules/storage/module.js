angular.module('nl.osseman.storage', ['ui.router','ui.calendar'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('root.storage', {
        url: "/storage",
        controller: "StorageIndexController",
        templateUrl: "storage/views/index.html",
        data : {
          authorizedRoles: []
        }
      });
  });
