angular.module('nl.osseman.todo').controller('TodoIndexController', ['$scope', '$resource', function($scope, $resource) {
  $scope.todo = [ {
    id : 1,
    title : "Fix my pc",
    done : false
  }, {
    id : 2,
    title : "Make a todo list",
    done : false
  }];
}]);
