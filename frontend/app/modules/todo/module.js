angular.module('nl.osseman.todo', ['ui.router'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('root.todo', {
        url: "/todo",
        controller: "TodoIndexController",
        templateUrl: "todo/views/index.html",
        data : {
          authorizedRoles: []
        }
      });
  });
