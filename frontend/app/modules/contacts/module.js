angular.module('nl.osseman.contacts', ['ui.router'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('root.dashboard', {
        url: "/",
        controller: "DashboardController",
        templateUrl: "dashboard/views/dashboard.html",
        data : {
          authorizedRoles: []
        }
      });
  });
