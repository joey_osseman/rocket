angular.module('nl.osseman.errorpages', ['ui.router'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('root.error', {
        url: "/error/404",
        controller: "ErrorController",
        templateUrl: "errorpages/views/404.html",
        data : {
          authorizedRoles: []
        }
      });
  });
