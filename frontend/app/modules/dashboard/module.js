angular.module('nl.osseman.dashboard', ['ui.router', 'nl.osseman.authentication'], function(){}).config(
  function($stateProvider, $urlRouterProvider, USER_ROLES) {
    $stateProvider
      .state('root.dashboard', {
        url: "/",
        controller: "DashboardController",
        templateUrl: "dashboard/views/dashboard.html",
        data: {
          authorizedRoles: []
        }
      });
  });
