angular.module('nl.osseman.email', ['ui.router'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('root.email', {
        url: "/email",
        controller: "EmailIndexController",
        templateUrl: "email/views/index.html",
        data : {
          authorizedRoles: []
        }
      });
  });
