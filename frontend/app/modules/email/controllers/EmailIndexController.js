angular.module('nl.osseman.email').controller('EmailIndexController', ['$scope', '$resource', 'baseurl', function($scope, $resource, baseurl) {
  var Email = $resource(baseurl + '/rest/email/:emailId', {emailId:'@id'});
Email.query(function(data) {
    $scope.emails = data;
  });

  /*  $scope.emails = [{
      _id: "randomid",
      subject: "I'm unread",
      summary: "I am an unread e-mail. You should read me...",
      tags: [],
      unread: true,
      dateReceived: new Date(),
      attachments: [
        {filename : "blabla.xlsx"}
      ]
    }, {
        _id: "randomid",
        subject: "Please approve invoice",
        summary: "Hello, I urge you approve my invoice immediately.",
        tags: [],
        unread: false,
        dateReceived: new Date()
      }, {
        _id: "randomid",
        subject: "Acceptance Test",
        summary: "Hello, some attachments I am sending you... Try downloading them..!",
        tags: [],
        unread: false,
        dateReceived: new Date(),
        attachments: [
          {filename : "blabla.xlsx"}
        ]
      }
    ];*/

    $scope.selectAllChange = function() {
        for(var i = 0; i < $scope.emails.length; i++) {
          $scope.emails[i].selected = $scope.selectAll;
        }
    };

    $scope.selectedChange = function(item){
      if(!item.selected && $scope.selectAll) {
        $scope.selectAll = false;
      } else if(item.selected && !$scope.selectAll) {
        var allSelected = true;
        for(var i = 0; i < $scope.emails.length; i++) {
          if(!$scope.emails[i].selected) {
            allSelected = false;
          }
        }

        $scope.selectAll = allSelected;
      }
    };

    $scope.downloadAttachment = function(attachment) {
      alert("'Downloading' "+ attachment.filename + "\r\n\r\n\r\n....Not really.");
    }
}]);
