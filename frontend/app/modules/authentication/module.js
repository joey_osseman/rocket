angular.module('nl.osseman.authentication', ['ui.router'], function(){}).config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
        url: "/login",
        controller: "LoginController",
        templateUrl: "authentication/views/login.html"
      });


  });
